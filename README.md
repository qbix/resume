# Getting Started with Resume React Template
## Putting your data
* Fill your data in `src/repositories/ResumeData.ts` file
* If you're missing some technologies, add them in `src/model/Technology.ts`
* If you're missing some technology group, add it to `src/model/TechnologyGroup.ts`
* You can modify the order in which the technologies are shown in the same file
* for technology tags color manipulation visit `src/components/elements/technologyTag.css`, the classes are suffixed
 with the technology group 'slug'
 
## Running the app
* Just run `npm install` and then `npm start` command
 
## Exporting to PDF
* Although there are some ways to export react app to PDF, for now the easiest way is just to print the page to PDF
 from your browser. Remember to select printing of the page background
