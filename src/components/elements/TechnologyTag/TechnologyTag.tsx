import React, {Component} from 'react';
import {Technology} from '../../../model';
import './technologyTag.css'

export class TechnologyTag extends Component<Props, any> {
    render() {
        return (
            <span className={`technology-tag group-${this.props.technology.group.slug}`}>
                {this.props.technology.name}
            </span>
        );
    }

}

interface Props {
    technology: Technology.Technology
}
