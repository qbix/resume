import React, {Component} from 'react';
import {WorkExperienceEntry} from '../../../model';
import {DateField} from './DateField';
import {CompanyField} from './CompanyField';
import {ResponsibilitiesField} from './ResponsibilitiesField';

export class WorkExperienceInfo extends Component<Props, any> {
    render() {
        const exp = this.props.experience;
        return (<div className="work-experience-entry">
            <DateField startDate={exp.startDate} endDate={exp.endDate}/>
            <CompanyField companyName={exp.companyName}/>
            <ResponsibilitiesField responsibilities={exp.responsibilities}/>
        </div>);
    }
}

interface Props {
    experience: WorkExperienceEntry
}
