import React, {Component} from 'react';

export class CompanyField extends Component<Props, any> {
    render() {
        return (
            <div className="company-name">
                {this.props.companyName}
            </div>
        );
    }
}

interface Props {
    companyName: string
}
