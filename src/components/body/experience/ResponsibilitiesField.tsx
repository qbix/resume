import React, {Component, ReactNode} from 'react';

export class ResponsibilitiesField extends Component<Props, any> {
    render() {
        return (
            <div className="responsibilities">
                {this.responsibilities()}
            </div>
        );
    }

    responsibilities = (): ReactNode[] => {
        return this.props.responsibilities
            .map(resp => <span>{resp}</span>);
    };
}

interface Props {
    responsibilities: string[]
}
