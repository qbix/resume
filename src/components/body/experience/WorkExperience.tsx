import React, {Component, ReactNode} from 'react';
import {WorkExperienceInfo} from './WorkExperienceInfo';
import {ResumeRepository} from '../../../repositories';
import {WorkExperienceEntry} from '../../../model';

export class WorkExperience extends Component<any, State> {
    state = {
        experience: []
    };

    constructor(params: any) {
        super(params);
        ResumeRepository.getExperience()
            .then(experience => this.setState({experience: experience}));
    }

    render() {
        return (<div className="work-experience">
            {this.workExperience()}
        </div>);
    }

    workExperience = (): ReactNode[] => {
        return this.state.experience
            .map(experience => <WorkExperienceInfo experience={experience}/>);
    };
}

interface State {
    experience: WorkExperienceEntry[]
}
