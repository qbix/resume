import React, {Component, ReactNode} from 'react';

export class DateField extends Component<Props, any> {
    render() {
        const par = this.props;
        return (
            <div className="date">
                {this.format(par.startDate)} - {this.format(par.endDate) || 'ongoing'}
            </div>
        );
    }

    format = (dt?: Date): ReactNode => {
        if (dt === undefined) return null;
        let month = (dt.getMonth() + 1).toString();
        if (month.length === 1) {
            month = `0${month}`;
        }

        return (<span>{`${dt.getFullYear()}-${month}`}</span>);
    };
}

interface Props {
    startDate: Date,
    endDate?: Date
}
