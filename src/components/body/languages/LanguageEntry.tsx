import React, {Component} from 'react';
import {Language} from '../../../model';

export class LanguageEntry extends Component<Props, any> {
    render() {
        const lang = this.props.language;
        return (
            <div className="language">
                <span className="language-name">{lang.name}</span> - <span
                className="language-level">{lang.level}</span>
            </div>
        );
    }
}

interface Props {
    language: Language
}
