import React, {Component, ReactNode} from 'react';
import {Language} from '../../../model';
import {ResumeRepository} from '../../../repositories';
import {LanguageEntry} from './LanguageEntry';

export class Languages extends Component<any, State> {
    state = {
        languages: []
    };

    constructor(params: any) {
        super(params);
        ResumeRepository.getLanguages()
            .then(languages => this.setState({languages: languages}));
    }

    render() {
        return (<div className="languages">{this.languages()}</div>);
    }

    languages = (): ReactNode[] => {
        return this.state.languages
            .map(lang => <LanguageEntry language={lang}/>);
    };
}

interface State {
    languages: Language[]
}
