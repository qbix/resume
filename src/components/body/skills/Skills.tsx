import React, {Component, ReactNode} from 'react';
import {SkillRankingEntry} from '../../../model';
import {skills} from '../../../repositories/ResumeData';
import {ResumeRepository} from '../../../repositories';
import {SkillInfo} from './SkillInfo';

export class Skills extends Component<any, State> {
    state = {
        skills: []
    };

    constructor(props: any) {
        super(props);
        ResumeRepository.getSkills()
            .then(skills => this.setState({skills: skills}));
    }

    render() {
        return (<div className="skills">{this.getSkills()}</div>);
    }

    getSkills = (): ReactNode[] => {
        return skills
            .sort((skill1, skill2) => skill1.technology.group.ordinal - skill2.technology.group.ordinal)
            .sort((skill1, skill2) => skill2.grade - skill1.grade)
            .map(skill => <SkillInfo skill={skill}/>);
    };
}

interface State {
    skills: SkillRankingEntry[]
}
