import React, {Component} from 'react';
import {SkillRankingEntry} from '../../../model';
import StarRatingComponent from 'react-star-rating-component';

export class SkillInfo extends Component<Props, any> {
    render() {
        const skill = this.props.skill;
        return (
            <div className="skill-info">
                {skill.technology.name}
                <span className="grade">
                <StarRatingComponent name={skill.technology.name} value={skill.grade} starCount={4}/>
                </span>
            </div>
        );
    }
}

interface Props {
    skill: SkillRankingEntry
}
