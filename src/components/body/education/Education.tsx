import React, {Component, ReactNode} from 'react';
import {EducationEntry} from '../../../model';
import {EducationInfo} from './EducationInfo';
import {ResumeRepository} from '../../../repositories';

export class Education extends Component<any, State> {
    state = {
        education: []
    };

    constructor(params: any) {
        super(params);
        ResumeRepository.getEducation()
            .then(edu => this.setState({education: edu}));
    }

    render() {
        return (<div className="education">
            {this.educationEntries()}
        </div>);
    }

    educationEntries = (): ReactNode[] => {
        return this.state.education
            .map(educationEntry => <EducationInfo entry={educationEntry}/>);
    };
}

interface State {
    education: EducationEntry[]
}
