import React, {Component} from 'react';
import {EducationEntry} from '../../../model';

export class EducationInfo extends Component<Props, any> {
    render() {
        const entry = this.props.entry;
        return (
            <div className="education-entry">
                {this.dates(entry)}
                {this.school(entry)}
                {this.details(entry)}
            </div>
        );
    }

    dates = (entry: EducationEntry) => {
        return <div className="dates">
            {entry.startYear} - {entry.endYear || 'ongoing'}
        </div>;
    };

    school = (entry: EducationEntry) => {
        return <div className="school-name">
            {entry.schoolName}
        </div>;
    };

    details = (entry: EducationEntry) => {
        return <div className="details">
            {entry.details}
        </div>;
    };
}

interface Props {
    entry: EducationEntry
}
