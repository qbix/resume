import React, {Component} from 'react';

export class Roles extends Component<Props, any> {
    render() {
        return (<div className="project-role">Role: {this.roles()}</div>);
    }

    roles = () => {
        return this.props.roles
            .join(', ');
    };
}

interface Props {
    roles: string[]
}
