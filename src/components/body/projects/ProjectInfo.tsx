import React, {Component} from 'react';
import {Project} from '../../../model';
import {Technologies} from './Technologies';
import {Roles} from './Roles';
import {Name} from './Name';

export class ProjectInfo extends Component<Props, any> {
    render() {
        return (
            <div className="project-info">
                <Name name={this.props.project.name}/>
                <Roles roles={this.props.project.roles}/>
                <Technologies technologies={this.props.project.technologies}/>
            </div>
        );
    }
}

interface Props {
    project: Project
}
