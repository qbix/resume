import React, {Component, ReactNode} from 'react';
import {ResumeRepository} from '../../../repositories';
import {Project} from '../../../model';
import {projects} from '../../../repositories/ResumeData';
import {ProjectInfo} from './ProjectInfo';
import './projects.css'

export class Projects extends Component<any, State> {
    state = {
        projects: []
    };

    constructor(props: any) {
        super(props);
        ResumeRepository.getProjects()
            .then(projects => this.setState({projects: projects}));
    }

    render() {
        return (<div className="projects">{this.projects()}</div>);
    }

    projects = (): ReactNode[] => {
        return projects
            .map(project => <ProjectInfo project={project}/>);
    };
}

interface State {
    projects: Project[]
}
