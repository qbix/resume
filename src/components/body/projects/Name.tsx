import React, {Component} from 'react';

export class Name extends Component<Props, any> {
    render() {
        return (<div className="project-name">{this.props.name}</div>);
    }
}

interface Props {
    name: string
}
