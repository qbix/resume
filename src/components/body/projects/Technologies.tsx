import React, {Component, ReactNode} from 'react';
import {Technology} from '../../../model';
import {TechnologyTag} from '../../elements';

export class Technologies extends Component<Props, any> {
    render() {
        return (<div className="project-technologies">{this.technologies()}</div>);
    }

    technologies = (): ReactNode[] => {
        return this.props.technologies
            .sort(Technology.compare)
            .map(technology => <TechnologyTag technology={technology}/>);
    };
}

interface Props {
    technologies: Technology.Technology[]
}
