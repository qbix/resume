import React, {Component, ReactNode} from 'react';
import './body.css';
import {Accordion, AccordionDetails, AccordionSummary} from '@material-ui/core';
import {Skills} from './skills';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Projects} from './projects/Projects';
import {WorkExperience} from './experience';
import {Education} from './education';
import {Languages} from './languages';

export class Body extends Component<any, any> {

    render() {
        return (<div className="body">
            {this.accordion('Skills', 'skills', <Skills/>, true)}
            {this.accordion('Projects', 'projects', <Projects/>)}
            {this.accordion('Work Experience', 'experience', <WorkExperience/>)}
            {this.accordion('Education', 'education', <Education/>)}
            {this.accordion('Languages', 'languages', <Languages/>)}
        </div>);
    }

    accordion = (name: string, slug: string, tag: ReactNode, expanded?: boolean) => {
        return (
            <Accordion defaultExpanded={expanded || false}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls={`${slug}-content`}
                    id={`${slug}-header`}
                    className="accordion-name">
                    <span className="section-name">{name}</span>
                </AccordionSummary>
                <AccordionDetails>
                    {tag}
                </AccordionDetails>
            </Accordion>);
    };
}

