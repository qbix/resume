import React, {Component} from 'react';
import {ResumeRepository} from '../../repositories';
import {PersonalInfo} from '../../model';
import './header.css';
import {Name} from './Name';
import {ContactDetails} from './ContactDetails';

export class Header extends Component<any, State> {

    state = {
        personalInfo: {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            email: '',
            resumeUrl: ''
        }
    };

    constructor(props: any) {
        super(props);
        ResumeRepository.getPersonalInfo()
            .then(personalInfo => this.setState({personalInfo: personalInfo}));
    }

    render() {
        const info = this.state.personalInfo;
        return (<div className="header">
            <Name firstName={info.firstName} lastName={info.lastName}/>
            <ContactDetails phoneNumber={info.phoneNumber} email={info.email} resumeUrl={info.resumeUrl}/>
        </div>);
    }
}

interface State {
    personalInfo: PersonalInfo
}
