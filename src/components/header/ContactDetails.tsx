import React, {Component} from 'react';

export class ContactDetails extends Component<Props, any> {
    render() {
        return (<div className="contact-details">
            <div>Phone number: {this.props.phoneNumber}</div>
            <div>E-mail: <a href={`mailto:${this.props.email}`}>{this.props.email}</a></div>
            {this.props.resumeUrl &&
            <div>Interactive version at: <a href={this.props.resumeUrl}>{this.props.resumeUrl}</a></div>}
        </div>);
    }
}

interface Props {
    phoneNumber: string
    email: string,
    resumeUrl?: string
}
