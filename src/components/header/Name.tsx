import React, {Component} from 'react';

export class Name extends Component<Props, any> {
    render() {
        return (<div className="name">
            {this.props.firstName} {this.props.lastName}
        </div>);
    }
}

interface Props {
    firstName: string
    lastName: string
}
