import React, {Component} from 'react';
import {Header} from './header';
import {PersonalInfo} from '../model';
import {Body} from './body';

export default class Canvas extends Component<any, State> {
    state = {
        personalInfo: {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            email: ''
        }
    };

    render() {
        return (<div className="canvas">
            <Header personalInfo={this.state.personalInfo}/>
            <Body/>
        </div>);
    }
}

interface State {
    personalInfo: PersonalInfo
}
