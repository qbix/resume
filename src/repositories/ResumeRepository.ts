import {EducationEntry, Language, PersonalInfo, Project, SkillRankingEntry, WorkExperienceEntry} from '../model';
import * as ResumeData from './ResumeData';

export async function getPersonalInfo(): Promise<PersonalInfo> {
    return ResumeData.personalInfo;
}

export async function getProjects(): Promise<Project[]> {
    return ResumeData.projects;
}

export async function getExperience(): Promise<WorkExperienceEntry[]> {
    return ResumeData.experience;
}

export async function getEducation(): Promise<EducationEntry[]> {
    return ResumeData.education;
}

export async function getSkills(): Promise<SkillRankingEntry[]> {
    return ResumeData.skills;
}

export async function getLanguages(): Promise<Language[]> {
    return ResumeData.languages;
}
