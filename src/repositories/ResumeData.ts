import {EducationEntry, Language, PersonalInfo, Project, SkillRankingEntry, WorkExperienceEntry} from '../model';
import {
    DOCKER,
    ECS,
    GRAYLOG,
    GROOVY,
    HIBERNATE,
    JAVA,
    JAVA11,
    JAVA8,
    JENKINS,
    JSF,
    KOTLIN,
    KUBERNETES,
    MARIA_DB,
    MAVEN,
    REACT,
    REDSHIFT,
    SPOCK,
    SPRING,
    TYPE_SCRIPT,
    VAADIN
} from '../model/Technology';

export const personalInfo: PersonalInfo = {
    firstName: 'John',
    lastName: 'Doe',
    phoneNumber: '+48 123 456 789',
    email: 'some@fake.mail',
    resumeUrl: 'http://url.to.resume'
};

export const projects: Project[] = [
    {
        name: 'Some Project Name',
        roles: ['First Role', 'Second Role'],
        technologies: [
            JAVA8, SPRING, SPOCK, HIBERNATE, MARIA_DB, ECS, JENKINS, MAVEN, KUBERNETES, DOCKER, GRAYLOG, VAADIN, JSF
        ]
    }, {
        name: 'Some other project',
        roles: ['Fullstack Developer'],
        technologies: [
            JAVA8, JAVA11, REACT, TYPE_SCRIPT, REDSHIFT, SPRING
        ]
    }
];

export const experience: WorkExperienceEntry[] = [
    {
        startDate: new Date('2019-01-01'),
        companyName: 'CompanyName',
        responsibilities: [
            'Doing some stuff',
            'And some more'
        ]
    },
    {
        startDate: new Date('2016-07-01'),
        endDate: new Date('2019-06-30'),
        companyName: 'Previous company',
        responsibilities: [
            'responsibilities',
            'they are really important'
        ]
    }
];

export const education: EducationEntry[] = [
    {
        startYear: 2019,
        schoolName: 'Some School',
        details: 'It was some trendy specialization'
    },
    {
        startYear: 2000,
        endYear: 2004,
        schoolName: 'Some secondary school',
        details: 'IT profiled class'
    }
];

export const skills: SkillRankingEntry[] = [
    {
        technology: JAVA,
        grade: 1
    }, {
        technology: KOTLIN,
        grade: 2
    }, {
        technology: GROOVY,
        grade: 3
    }
];

export const languages: Language[] = [
    {
        name: 'Polish',
        level: 'Native'
    },
    {
        name: 'English',
        level: 'Upper intermediate'
    }
];


