export interface WorkExperienceEntry {
    startDate: Date,
    endDate?: Date,
    companyName: string,
    responsibilities: string[]
}
