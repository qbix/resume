import {Technology} from './Technology';

export interface Project {
    name: string,
    roles: string[],
    technologies: Technology[]
}
