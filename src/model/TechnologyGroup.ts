export interface TechnologyGroup {
    name: string,
    slug: string
    ordinal: number
}

export const BACKEND: TechnologyGroup = {
    name: 'Backend Languages',
    slug: 'backend',
    ordinal: 1
};

export const FRONTEND: TechnologyGroup = {
    name: 'Frontend Technologies',
    slug: 'frontend',
    ordinal: 4
};

export const AWS: TechnologyGroup = {
    name: 'AWS',
    slug: 'aws',
    ordinal: 5
};

export const BACKEND_LIBS: TechnologyGroup = {
    name: 'Backend libraries',
    slug: 'backend-libs',
    ordinal: 3
};

export const DEPLOYMENT: TechnologyGroup = {
    name: 'Deployment tools',
    slug: 'deployment',
    ordinal: 6
};

export const TESTING: TechnologyGroup = {
    name: 'Testing tools',
    slug: 'testing',
    ordinal: 7
};

export const DB: TechnologyGroup = {
    name: 'Database systems',
    slug: 'db',
    ordinal: 8
};

export const BUILD_TOOLS: TechnologyGroup = {
    name: 'Build tools',
    slug: 'build',
    ordinal: 9
};

export const OTHER: TechnologyGroup = {
    name: 'Other',
    slug: 'other',
    ordinal: 9999
};
