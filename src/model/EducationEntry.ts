export interface EducationEntry {
    startYear: number
    endYear?: number
    schoolName: string
    details: string
}
