export interface PersonalInfo {
    firstName: string
    lastName: string
    phoneNumber: string
    email: string
    resumeUrl?: string
}
