import * as TechnologyGroup from './TechnologyGroup';

export interface Technology {
    name: string
    group: TechnologyGroup.TechnologyGroup
}

export const GROOVY: Technology = {
    name: 'Groovy',
    group: TechnologyGroup.BACKEND
};

export const KOTLIN: Technology = {
    name: 'Kotlin',
    group: TechnologyGroup.BACKEND
};

export const JAVA: Technology = {
    name: 'Java',
    group: TechnologyGroup.BACKEND
};

export const JAVA6: Technology = {
    name: 'Java 6',
    group: TechnologyGroup.BACKEND
};

export const JAVA8: Technology = {
    name: 'Java 8',
    group: TechnologyGroup.BACKEND
};

export const JAVA11: Technology = {
    name: 'Java 11',
    group: TechnologyGroup.BACKEND
};

export const GWT: Technology = {
    name: 'GWT',
    group: TechnologyGroup.FRONTEND
};

export const JSF: Technology = {
    name: 'JSF',
    group: TechnologyGroup.FRONTEND
};

export const JAVA_SCRIPT: Technology = {
    name: 'Java Script',
    group: TechnologyGroup.FRONTEND
};

export const J_QUERY: Technology = {
    name: 'jQuery',
    group: TechnologyGroup.FRONTEND
};

export const REACT: Technology = {
    name: 'React',
    group: TechnologyGroup.FRONTEND
};

export const TYPE_SCRIPT: Technology = {
    name: 'Type Script',
    group: TechnologyGroup.FRONTEND
};

export const VAADIN: Technology = {
    name: 'Vaadin',
    group: TechnologyGroup.FRONTEND
};

export const REDSHIFT: Technology = {
    name: 'Amazon Redshift',
    group: TechnologyGroup.AWS
};

export const ECS: Technology = {
    name: 'Amazon ECS',
    group: TechnologyGroup.AWS
};

export const RDS: Technology = {
    name: 'Amazon RDS',
    group: TechnologyGroup.AWS
};

export const HIBERNATE: Technology = {
    name: 'Hibernate',
    group: TechnologyGroup.BACKEND_LIBS
};
export const SPRING: Technology = {
    name: 'Spring',
    group: TechnologyGroup.BACKEND_LIBS
};
export const QUERY_DSL: Technology = {
    name: 'QueryDSL',
    group: TechnologyGroup.BACKEND_LIBS
};

export const SPOCK: Technology = {
    name: 'Spock',
    group: TechnologyGroup.TESTING
};

export const TEST_NG: Technology = {
    name: 'TestNg',
    group: TechnologyGroup.TESTING
};

export const MARIA_DB: Technology = {
    name: 'MariaDB',
    group: TechnologyGroup.DB
};

export const MY_SQL: Technology = {
    name: 'MySQL',
    group: TechnologyGroup.DB
};

export const ORACLE: Technology = {
    name: 'Oracle',
    group: TechnologyGroup.DB
};

export const POSTGRES: Technology = {
    name: 'PostgreSQL',
    group: TechnologyGroup.DB
};

export const REDIS: Technology = {
    name: 'Redis',
    group: TechnologyGroup.DB
};

export const DOCKER: Technology = {
    name: 'Docker',
    group: TechnologyGroup.DEPLOYMENT
};

export const KUBERNETES: Technology = {
    name: 'Kubernetes',
    group: TechnologyGroup.DEPLOYMENT
};

export const TOMCAT: Technology = {
    name: 'Tomcat',
    group: TechnologyGroup.DEPLOYMENT
};

export const JENKINS: Technology = {
    name: 'Jenkins',
    group: TechnologyGroup.DEPLOYMENT
};

export const MAVEN: Technology = {
    name: 'Maven',
    group: TechnologyGroup.BUILD_TOOLS
};

export const GRADLE: Technology = {
    name: 'Gradle',
    group: TechnologyGroup.BUILD_TOOLS
};

export const GRAYLOG: Technology = {
    name: 'Graylog',
    group: TechnologyGroup.OTHER
};

export const ELASTICSEARCH: Technology = {
    name: 'Elasticsearch',
    group: TechnologyGroup.OTHER
};

export const PENTAHO_REPORTING: Technology = {
    name: 'Pentaho Reporting Engine',
    group: TechnologyGroup.OTHER
};

export const PENTAHO_DATA_INTEGRATION: Technology = {
    name: 'Pentaho DI',
    group: TechnologyGroup.OTHER
};

export const HIGHCHARTS: Technology = {
    name: 'Highcharts',
    group: TechnologyGroup.OTHER
};

export const PENTAHO_BI: Technology = {
    name: 'Pentaho BI',
    group: TechnologyGroup.OTHER
};

export const MONDRIAN: Technology = {
    name: 'Mondrian',
    group: TechnologyGroup.OTHER
};

export const compare = (t1: Technology, t2: Technology): number => {
    const groupCompare = t1.group.ordinal - t2.group.ordinal;
    if (groupCompare === 0) {
        return t1.name.localeCompare(t2.name);
    } else {
        return groupCompare;
    }
};



