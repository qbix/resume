import {Technology} from './Technology';

export interface SkillRankingEntry {
    technology: Technology
    grade: number
    remarks?: string
}
